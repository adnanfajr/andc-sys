<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\GigiRequest as StoreRequest;
use App\Http\Requests\GigiRequest as UpdateRequest;

class RekapGigiCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setModel("App\PoliGigi");
        $this->crud->setRoute("rekap/gigi");
        $this->crud->setEntityNameStrings('rekapitulasi gigi', 'rekapitulasi gigi');

        $bulan = \Route::current()->parameter('bulan');
        $this->crud->addClause('whereMonth', 'tanggal', '=', $bulan);

        $tahun = \Route::current()->parameter('tahun');
        $this->crud->addClause('whereYear', 'tanggal', '=', $tahun);
        
        $this->crud->orderBy('tanggal', 'ASC');
        $this->crud->removeAllButtons();
        $this->crud->removeColumn('action');
        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->enableExportButtons();;

        /*
            Field diagnosa dibikin lebih dari satu diagnosa, atau kayak gitu gakpapa tapi dibikin rekap hanya terhitung sekali.
        */

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'tanggal',
                                'label' => 'Tanggal Periksa',
                                'type' => 'date',
                                'value' => date('d F Y')
                            ]);
        $this->crud->addColumn([
                            'label' => 'Tenaga Medis',
                            'type' => 'model_function',
                            'function_name' => 'NamaDokter',
                            'name' => 'dok'
                            ]);
        $this->crud->addColumn([
                                'name' => 'chart',
                                'label' => "Odontogram"
                            ]);
        $this->crud->addColumn([
                            'label' => 'Diagnosa',
                            'type' => 'model_function',
                            'function_name' => 'NamaDiagnosa',
                            'name' => 'diag'
                            ]);
        $this->crud->addColumn([
                                'name' => 'tindakan',
                                'label' => "Tindakan"
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date',
                                'value' => date('Y-m-d')
                            ], 'create');
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date'
                            ], 'update');
        $this->crud->addField([ // 1-1 relationship
                                'label' => "Dokter", // Table column heading
                                'type' => "select2_from_ajax",
                                'name' => 'dokter', // the column that contains the ID of that connected entity
                                'entity' => 'tenagamedis', // the method that defines the relationship in your Model
                                'attribute' => "nama", // foreign key attribute that is shown to user
                                'model' => "App\TenagaMedis", // foreign key model
                                'data_source' => url("api/tenagamedis"), // url to controller search function (with /{id} should return model)
                                'placeholder' => "Pilih Dokter", // placeholder for the select
                                'minimum_input_length' => 2, // minimum characters to type before querying results
                                ]);
        $this->crud->addField([
                                // Custom Field
                                'name' => 'chart',
                                'label' => 'Odontogram',
                                'type' => 'textodo'
                                ]);
        $this->crud->addField([ // 1-n relationship
                                'label' => "Diagnosa", // Table column heading
                                'type' => "select2_from_ajax",
                                //'type' => "select2_from_ajax_multiple",
                                //'type' => 'select2_multiple',
                                'name' => 'diagnosa', // the column that contains the ID of that connected entity
                                'entity' => 'diagnosagigi', // the method that defines the relationship in your Model
                                'attribute' => "deskripsi", // foreign key attribute that is shown to user
                                'model' => "App\DiagnosaGigi", // foreign key model
                                'data_source' => url("api/diagnosagigi"), // url to controller search function (with /{id} should return model)
                                'placeholder' => "Pilih Diagnosa", // placeholder for the select
                                'minimum_input_length' => 2, // minimum characters to type before querying results
                                //'pivot' => true,
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'tindakan',
                                'label' => 'Tindakan',
                                'type' => 'textarea',
                                'placeholder' => 'Your textarea text here'
                            ]);
        /*
        $this->crud->addField([    // WYSIWYG
                                'name' => 'chart',
                                'label' => 'Odontogram',
                                'type' => 'text',
                                'placeholder' => 'Your textarea text here'
                            ]);
        */
    }

	public function store(StoreRequest $request)
	{
		$request['pasien_id'] = \Route::current()->parameter('pasien');

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}