<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PasienRequest as StoreRequest;
use App\Http\Requests\PasienRequest as UpdateRequest;

class PasienCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\Pasien");
        $this->crud->setRoute("admin/pasien");
        $this->crud->setEntityNameStrings('pasiens', 'pasiens');
        $this->crud->orderBy('id', 'desc');

        $this->crud->setDefaultPageLength(10);
        $this->crud->removeButton('create');
        //$this->crud->addButtonFromView('line', 'open_google', 'update', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'open_gigi', 'kontrolGigi', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'open_lab', 'kontrolLab', 'beginning');
        $this->crud->addButtonFromModelFunction('line', 'open_kia', 'kontrolKIA', 'beginning');

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'no_rekam_medis',
                                'label' => "No. Rekam Medis"
                            ]);
        $this->crud->addColumn([
                                'name' => 'nama',
                                'label' => "Nama Pasien"
                            ]);
        $this->crud->addColumn([
                                'name' => 'suami',
                                'label' => "Nama Suami"
                            ]);
        $this->crud->addColumn([
                                'name' => 'ttl',
                                'label' => 'Tanggal Lahir',
                                'type' => 'date',
                                'value' => date('d F Y')
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'nama',
                                'label' => 'Nama',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'suami',
                                'label' => 'Nama Suami',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'pendidikan',
                                'label' => 'Pendidikan Terakhir',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
                            /*
        $this->crud->addField([
                                'name' => 'slug',
                                'label' => "Slug (URL)",
                                'type' => 'text',
                                'hint' => 'Will be automatically generated from your title, if left empty.'
                                // 'disabled' => 'disabled'
                            ]);
                            */
        $this->crud->addField([    // TEXT
                                'name' => 'tl',
                                'label' => 'Tempat Lahir',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'ttl',
                                'label' => 'Tanggal Lahir',
                                'type' => 'date',
                                'value' => date('Y-m-d')
                            ], 'create');
        $this->crud->addField([    // TEXT
                                'name' => 'ttl',
                                'label' => 'Tanggal Lahir',
                                'type' => 'date'
                            ], 'update');

        $this->crud->addField([    // WYSIWYG
                                'name' => 'alamat',
                                'label' => 'Alamat',
                                'type' => 'textarea',
                                'placeholder' => 'Your textarea text here'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'agama',
                                'label' => "Agama",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'usia',
                                'label' => 'Usia',
                                'type' => 'number',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'telp',
                                'label' => 'Nomor Telepon',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'no_rekam_medis',
                                'label' => 'Nomor Rekam Medis',
                                'type' => 'text',
                                'placeholder' => 'Your textarea text here'
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'nik',
                                'label' => 'NIK',
                                'type' => 'number',
                                'placeholder' => 'Your textarea text here'
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'no_bpjs',
                                'label' => 'Nomor BPJS',
                                'type' => 'number',
                                'placeholder' => 'Your textarea text here'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}