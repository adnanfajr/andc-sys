<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\KIARequest as StoreRequest;
use App\Http\Requests\KIARequest as UpdateRequest;

use App\Pasien;
use App\PoliKIA;
use App\PoliGigi;

class KIACrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        $pasien_id = \Route::current()->parameter('pasien');
        $pasien = Pasien::where('id', $pasien_id)->first();

        $user = PoliGigi::where('pasien_id', $pasien_id)->first();

        if($user === null){
            flash('<b>PERHATIAN</b>: '.$pasien->nama.' belum pernah periksa ke Poli Gigi')->error();
        }

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setModel("App\PoliKIA");
        $this->crud->setRoute("admin/pasien/kontrol/".$pasien_id."/kia");
        $this->crud->setEntityNameStrings('data periksa KIA untuk '.$pasien->nama, 'POLI KIA - '.$pasien->nama);

        $this->crud->orderBy('tanggal', 'ASC');
        $this->crud->addClause('where', 'pasien_id', '=', $pasien_id);
        $this->crud->setDefaultPageLength(10);
        $this->crud->enableExportButtons();

        //$this->crud->addButtonFromModelFunction('line', 'view_kia', 'view');
        //$this->crud->addButtonFromModelFunction('bottom', 'poli_lain', 'poli');
        $this->crud->addButtonFromView('bottom', 'back', 'back');

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        /*
        $this->crud->addColumn([
            'label' => 'Kunjungan ke-',
            'type' => 'model_function',
            'function_name' => 'nomor',
            'name' => 'nomor'
        ]);
        */
        $this->crud->addColumn([
                                'name' => 'tanggal',
                                'label' => 'Tanggal Periksa',
                                'type' => 'date',
                                'value' => date('d F Y')
                            ]);
        $this->crud->addColumn([
                                'name' => 'k',
                                'label' => "K"
                            ]);
        $this->crud->addColumn([
                                'label' => 'Tenaga Medis',
                                'type' => 'model_function',
                                'function_name' => 'NamaDokter',
                                'name' => 'dok'
                            ]);
        $this->crud->addColumn([
                                'name' => 'kehamilan',
                                'label' => "Kehamilan ke-"
                            ]);
        $this->crud->addColumn([
                                'name' => 'preeklamsi',
                                'label' => 'Riwayat Preeklamsi',
                                'type' => 'model_function',
                                'function_name' => 'Preeklamsi',
                            ]);
        $this->crud->addColumn([
                                'name' => 'keluhan',
                                'label' => "Keluhan"
                            ]);
        $this->crud->addColumn([
                                'name' => 'bb',
                                'label' => "BB Ibu (Kg)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'bb_bayi',
                                'label' => "BB Bayi (Kg)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'td',
                                'label' => "TD (mmHg)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'nadi',
                                'label' => "Nadi"
                            ]);
        $this->crud->addColumn([
                                'name' => 'rr',
                                'label' => "RR"
                            ]);
        $this->crud->addColumn([
                                'name' => 'tfu',
                                'label' => "TFU (cm)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'umur',
                                'label' => "Umur Kehamilan (minggu)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'skor',
                                'label' => "SKOR (KSPR)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'kesimpulan',
                                'label' => "Kesimpulan"
                            ]);
        $this->crud->addColumn([
                                'name' => 'rujuk',
                                'label' => "Rujuk ke-"
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date',
                                'value' => date('Y-m-d')
                            ], 'create');
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date'
                            ], 'update');
        $this->crud->addField([ // 1-1 relationship
                                'label' => "Dokter", // Table column heading
                                'type' => "select2_from_ajax",
                                'name' => 'dokter', // the column that contains the ID of that connected entity
                                'entity' => 'tenagamedis', // the method that defines the relationship in your Model
                                'attribute' => "nama", // foreign key attribute that is shown to user
                                'model' => "App\TenagaMedis", // foreign key model
                                'data_source' => url("api/tenagamedis"), // url to controller search function (with /{id} should return model)
                                'placeholder' => "Pilih Dokter", // placeholder for the select
                                'minimum_input_length' => 2, // minimum characters to type before querying results
                                ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'kehamilan',
                                'label' => 'Kehamilan ke-',
                                'type' => 'text',
                                'placeholder' => 'Your textarea text here'
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'preeklamsi',
                                'label' => 'Riwayat Preeklamsi Kehamilan Sebelumnya',
                                'type' => 'checkbox'
                            ]);
        $this->crud->addField([    // WYSIWYG
                                'name' => 'keluhan',
                                'label' => 'Keluhan',
                                'type' => 'textarea',
                                'placeholder' => 'Your textarea text here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'bb',
                                'label' => 'Berat Badan Ibu',
                                'type' => 'number',
                                'suffix' => "Kg",
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'bb_bayi',
                                'label' => 'Berat Badan Bayi',
                                'type' => 'number',
                                'suffix' => "Kg",
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'td',
                                'label' => 'Tekanan Darah',
                                'type' => 'text',
                                'suffix' => "mmHg",
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'nadi',
                                'label' => 'Denyut Nadi',
                                'type' => 'number',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'rr',
                                'label' => 'RR',
                                'type' => 'number',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'tfu',
                                'label' => 'TFU',
                                'type' => 'number',
                                'suffix' => "cm",
                                'attributes' => ["step" => "any"], // allow decimals
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'umur',
                                'label' => 'Umur Kehamilan',
                                'type' => 'number',
                                'suffix' => "minggu",
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'skor',
                                'label' => 'SKOR (KSPR)',
                                'type' => 'number',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'kesimpulan',
                                'label' => 'Kesimpulan (Dx)',
                                'type' => 'textarea',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'rujuk',
                                'label' => 'Rujuk ke-',
                                'type' => 'text', // dibikin select ajax
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'status',
                                'label' => "Status",
                                'type' => 'enum'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		$request['pasien_id'] = \Route::current()->parameter('pasien');

        $k = "K";
        if(('0' <= $request->umur) && ($request->umur <= '12'))
        {
            $k = 'K1';
        }
        elseif(('13' <= $request->umur) && ($request->umur <= '27'))
        {
            $k = 'K2';
        }
        elseif(('28' <= $request->umur) && ($request->umur <= '40'))
        {
            if(PoliKIA::where('pasien_id', \Route::current()->parameter('pasien'))->where('k', 'like', '%K3%')->exists())
            {   
                $k = 'K4';
            }
            else
            {
                $k = 'K3';
            }
        }

        $request['k'] = $k;

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}