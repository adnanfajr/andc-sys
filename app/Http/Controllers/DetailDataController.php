<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\PoliKIA;
use App\PoliGigi;
use App\PoliLab;

class DetailDataController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pasien_id = \Route::current()->parameter('pasien');
        $poli = \Route::current()->parameter('poli');
        $data_id = \Route::current()->parameter('data');

        $nama = Pasien::where('id', $pasien_id)->first();

        if($poli==="kia"){
            $data = PoliKIA::where('pasien_id', $pasien_id)->where('id', $data_id)->get();
            $detail['data'] = $data;

            return view('detail', array('data' => $detail));
        }
        elseif($poli==="gigi"){
            $data = PoliGigi::where('pasien_id', $pasien_id)->where('id', $data_id)->get();
            $detail['data'] = $data;

            return view('detail', array('data' => $detail));
        }
        elseif($poli==="lab"){
            $data = PoliLab::where('pasien_id', $pasien_id)->where('id', $data_id)->get();
            $detail['data'] = $data;

            return view('detail', array('data' => $detail));
        }
        else{
            return view('errors.404');
        }

    }

}