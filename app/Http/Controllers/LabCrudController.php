<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LabRequest as StoreRequest;
use App\Http\Requests\LabRequest as UpdateRequest;

use App\Pasien;
use App\PoliGigi;

class LabCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        $pasien_id = \Route::current()->parameter('pasien');
        $pasien = Pasien::where('id', $pasien_id)->first();

        $user = PoliGigi::where('pasien_id', $pasien_id)->first();

        if($user === null){
            flash('<b>PERHATIAN</b>: '.$pasien->nama.' belum pernah periksa ke Poli Gigi')->error();
        }

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $pasien_id = \Route::current()->parameter('pasien');
        $pasien = Pasien::where('id', $pasien_id)->first();

        $this->crud->setModel("App\PoliLab");
        $this->crud->setRoute("admin/pasien/kontrol/".$pasien_id."/lab");
        $this->crud->setEntityNameStrings('data periksa Lab untuk '.$pasien->nama, 'LABORATORIUM - '.$pasien->nama);

        $this->crud->addClause('where', 'pasien_id', '=', $pasien_id);
        $this->crud->setDefaultPageLength(10);
        $this->crud->enableExportButtons();

        //$this->crud->addButtonFromModelFunction('line', 'view_lab', 'view');
        //$this->crud->addButtonFromModelFunction('bottom', 'poli_lain1', 'poli');
        $this->crud->addButtonFromView('bottom', 'back1', 'back');

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'tanggal',
                                'label' => 'Tanggal Periksa',
                                'type' => 'date',
                                'value' => date('d F Y')
                            ]);
        $this->crud->addColumn([
                                'name' => 'hb',
                                'label' => "Hb (mg/dL)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'goldar',
                                'label' => "Golongan Darah"
                            ]);
        $this->crud->addColumn([
                                'name' => 'albumin',
                                'label' => "Albumin"
                            ]);
        $this->crud->addColumn([
                                'name' => 'reduksi',
                                'label' => "Reduksi"
                            ]);
        $this->crud->addColumn([
                                'name' => 'gula',
                                'label' => "Gula Darah"
                            ]);
        $this->crud->addColumn([
                                'name' => 'urin',
                                'label' => "Urin"
                            ]);
        $this->crud->addColumn([
                                'name' => 'sifilis',
                                'label' => "Sifilis"
                            ]);
        $this->crud->addColumn([
                                'name' => 'hbsag',
                                'label' => "HbsAg"
                            ]);
        $this->crud->addColumn([
                                'name' => 'hiv',
                                'label' => "HIV"
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date',
                                'value' => date('Y-m-d')
                            ], 'create');
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date'
                            ], 'update');
        $this->crud->addField([    // TEXT
                                'name' => 'hb',
                                'label' => 'Hb',
                                'type' => 'number',
                                'placeholder' => 'Your title here',
                                'suffix' => "mg/dL",
                                'attributes' => ["step" => "any"], // allow decimals
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'goldar',
                                'label' => "Golongan Darah",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'albumin',
                                'label' => 'Albumin',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'reduksi',
                                'label' => 'Reduksi',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'gula',
                                'label' => 'Gula Darah',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                                'suffix' => "mg/dL",
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'urin',
                                'label' => 'Urin',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'sifilis',
                                'label' => "Sifilis",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'hbsag',
                                'label' => "HbsAg",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'hiv',
                                'label' => "HIV",
                                'type' => 'enum'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		$request['pasien_id'] = \Route::current()->parameter('pasien');

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}