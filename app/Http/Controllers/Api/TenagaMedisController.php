<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TenagaMedis;

class TenagaMedisController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = TenagaMedis::where('nama', 'LIKE', '%'.$search_term.'%')->paginate(10);
            //$results = TenagaMedis::where('nama', 'LIKE', '%'.$search_term.'%')->where('pekerjaan', 'Dokter')->orWhere('pekerjaan', 'Dokter Gigi')->paginate(10);
        }
        else
        {
            $results = TenagaMedis::paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return TenagaMedis::find($id);
    }
}