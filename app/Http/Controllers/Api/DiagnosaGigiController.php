<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DiagnosaGigi;

class DiagnosaGigiController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');
        $page = $request->input('page');

        if ($search_term)
        {
            $results = DiagnosaGigi::where('deskripsi', 'LIKE', '%'.$search_term.'%')->paginate(10);
        }
        else
        {
            $results = DiagnosaGigi::paginate(10);
        }

        return $results;
    }

    public function show($id)
    {
        return DiagnosaGigi::find($id);
    }
}