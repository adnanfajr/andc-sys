<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TenagaMedisRequest as StoreRequest;
use App\Http\Requests\TenagaMedisRequest as UpdateRequest;

use App\Pasien;
use App\PoliGigi;

class TenagaMedisCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        // Dokter, Dokter Gigi, Bidan

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setModel("App\TenagaMedis");
        $this->crud->setRoute("admin/tenagamedis");
        $this->crud->setEntityNameStrings('tenaga medis', 'tenaga medis');

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'nama',
                                'label' => 'Nama'
                            ]);
        $this->crud->addColumn([
                                'name' => 'pekerjaan',
                                'label' => 'Pekerjaan'
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'nama',
                                'label' => 'Nama Dokter',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'pekerjaan',
                                'label' => "Pekerjaan",
                                'type' => 'enum'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}