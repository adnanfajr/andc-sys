<?php 

// Override AdminController vendor Backpack

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\PoliKIA;
use App\PoliGigi;
use App\PoliLab;

class DashboardController extends Controller
{
    protected $data = []; // the information we send to the view
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $this->data['title'] = trans('backpack::base.dashboard'); // set the page title

        // Data untuk grafik
        $kia_hari = PoliKIA::whereRaw('DAY(tanggal) = DAY(NOW())')->count();
        $kia_minggu = PoliKIA::whereRaw('WEEK(tanggal) = WEEK(NOW())')->count();
        $kia_bulan = PoliKIA::whereRaw('MONTH(tanggal) = MONTH(NOW())')->count();

        $lab_hari = PoliLab::whereRaw('DAY(tanggal) = DAY(NOW())')->count();
        $lab_minggu = PoliLab::whereRaw('WEEK(tanggal) = WEEK(NOW())')->count();
        $lab_bulan = PoliLab::whereRaw('MONTH(tanggal) = MONTH(NOW())')->count();

        $gigi_hari = PoliGigi::whereRaw('DAY(tanggal) = DAY(NOW())')->count();
        $gigi_minggu = PoliGigi::whereRaw('WEEK(tanggal) = WEEK(NOW())')->count();
        $gigi_bulan = PoliGigi::whereRaw('MONTH(tanggal) = MONTH(NOW())')->count();

        return view('dashboard', compact($this->data, 'kia_hari', 'kia_minggu', 'kia_bulan', 'lab_hari', 'lab_minggu', 'lab_bulan', 'gigi_hari', 'gigi_minggu', 'gigi_bulan'));
    }

    /**
     * Redirect to the dashboard.
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function redirect()
    {
        // The '/admin' route is not to be used as a page, because it breaks the menu's active state.
        return redirect(config('backpack.base.route_prefix').'/dashboard');
    }

    public function rekap()
    {
        $this->data['title'] = 'Rekapitulasi'; // set the page title

        // Data untuk grafik
        $tahun = \Route::current()->parameter('tahun');

        $kia_jan = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_feb = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_mar = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_apr = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_mei = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_juni = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_juli = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_agus = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_sept = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_okt = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_nov = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $kia_des = PoliKIA::where('k', 'K1')->whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

        $gigi_jan = PoliGigi::whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_feb = PoliGigi::whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_mar = PoliGigi::whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_apr = PoliGigi::whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_mei = PoliGigi::whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_juni = PoliGigi::whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_juli = PoliGigi::whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_agus = PoliGigi::whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_sept = PoliGigi::whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_okt = PoliGigi::whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_nov = PoliGigi::whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $gigi_des = PoliGigi::whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

        $lab_jan = PoliLab::whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_feb = PoliLab::whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_mar = PoliLab::whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_apr = PoliLab::whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_mei = PoliLab::whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_juni = PoliLab::whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_juli = PoliLab::whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_agus = PoliLab::whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_sept = PoliLab::whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_okt = PoliLab::whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_nov = PoliLab::whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
        $lab_des = PoliLab::whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

        return view('grafik_all', compact( 'tahun',
            'kia_jan','kia_feb','kia_mar','kia_apr','kia_mei','kia_juni',
            'kia_juli','kia_agus','kia_sept','kia_okt','kia_nov','kia_des',
            'lab_jan','lab_feb','lab_mar','lab_apr','lab_mei','lab_juni',
            'lab_juli','lab_agus','lab_sept','lab_okt','lab_nov','lab_des',
            'gigi_jan','gigi_feb','gigi_mar','gigi_apr','gigi_mei','gigi_juni',
            'gigi_juli','gigi_agus','gigi_sept','gigi_okt','gigi_nov','gigi_des'
            ));
    }

    public function rekap_poli()
    {
        $this->data['title'] = 'Rekapitulasi'; // set the page title

        // Data untuk grafik
        $poli = \Route::current()->parameter('poli');
        $tahun = \Route::current()->parameter('tahun');

        if($poli=="kia")
        {
            $jan = PoliKIA::whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $feb = PoliKIA::whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mar = PoliKIA::whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $apr = PoliKIA::whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mei = PoliKIA::whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juni = PoliKIA::whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juli = PoliKIA::whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $agus = PoliKIA::whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $sept = PoliKIA::whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $okt = PoliKIA::whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $nov = PoliKIA::whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $des = PoliKIA::whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

            return view('grafik_poli', compact( 'poli', 'tahun',
            'jan','feb','mar','apr','mei','juni',
            'juli','agus','sept','okt','nov','des'
            ));
        }
        elseif($poli=="gigi")
        {
            $jan = PoliGigi::whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $feb = PoliGigi::whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mar = PoliGigi::whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $apr = PoliGigi::whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mei = PoliGigi::whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juni = PoliGigi::whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juli = PoliGigi::whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $agus = PoliGigi::whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $sept = PoliGigi::whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $okt = PoliGigi::whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $nov = PoliGigi::whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $des = PoliGigi::whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

            return view('grafik_poli', compact( 'poli', 'tahun',
            'jan','feb','mar','apr','mei','juni',
            'juli','agus','sept','okt','nov','des'
            ));
        }
        elseif($poli=="lab")
        {
            $jan = PoliLab::whereRaw('MONTH(tanggal) = 1')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $feb = PoliLab::whereRaw('MONTH(tanggal) = 2')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mar = PoliLab::whereRaw('MONTH(tanggal) = 3')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $apr = PoliLab::whereRaw('MONTH(tanggal) = 4')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $mei = PoliLab::whereRaw('MONTH(tanggal) = 5')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juni = PoliLab::whereRaw('MONTH(tanggal) = 6')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $juli = PoliLab::whereRaw('MONTH(tanggal) = 7')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $agus = PoliLab::whereRaw('MONTH(tanggal) = 8')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $sept = PoliLab::whereRaw('MONTH(tanggal) = 9')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $okt = PoliLab::whereRaw('MONTH(tanggal) = 10')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $nov = PoliLab::whereRaw('MONTH(tanggal) = 11')->whereRaw('YEAR(tanggal) = '.$tahun)->count();
            $des = PoliLab::whereRaw('MONTH(tanggal) = 12')->whereRaw('YEAR(tanggal) = '.$tahun)->count();

            return view('grafik_poli', compact( 'poli', 'tahun',
            'jan','feb','mar','apr','mei','juni',
            'juli','agus','sept','okt','nov','des'
            ));
        }
    }

    public function rekapKIA()
    {
        $this->data['title'] = 'Rekapitulasi Poli KIA'; // set the page title
    }

    public function rekapLab()
    {
        $this->data['title'] = 'Rekapitulasi Laboratorium'; // set the page title
    }

    public function rekapGigi()
    {
        $this->data['title'] = 'Rekapitulasi Poli Gigi'; // set the page title
    }
}