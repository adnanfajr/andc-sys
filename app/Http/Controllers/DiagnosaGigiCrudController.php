<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DiagnosaGigiRequest as StoreRequest;
use App\Http\Requests\DiagnosaGigiRequest as UpdateRequest;

use App\Pasien;
use App\PoliGigi;

class DiagnosaGigiCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

        $this->crud->setModel("App\DiagnosaGigi");
        $this->crud->setRoute("admin/diagnosagigi");
        $this->crud->setEntityNameStrings('diagnosa gigi', 'diagnosa gigi');

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'kode',
                                'label' => 'Kode ICD-10'
                            ]);
        $this->crud->addColumn([
                                'name' => 'deskripsi',
                                'label' => 'Deskripsi'
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'kode',
                                'label' => 'Kode ICD-10',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'deskripsi',
                                'label' => 'Deskripsi',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}