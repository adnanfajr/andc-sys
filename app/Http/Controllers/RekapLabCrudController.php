<?php 

namespace App\Http\Controllers;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\LabRequest as StoreRequest;
use App\Http\Requests\LabRequest as UpdateRequest;

class RekapLabCrudController extends CrudController {

	public function __construct() {
        parent::__construct();

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("App\PoliLab");
        $this->crud->setRoute("rekap/lab");
        $this->crud->setEntityNameStrings('rekapitulasi lab', 'rekapitulasi lab');

        $bulan = \Route::current()->parameter('bulan');
        $this->crud->addClause('whereMonth', 'tanggal', '=', $bulan);
        
        $tahun = \Route::current()->parameter('tahun');
        $this->crud->addClause('whereYear', 'tanggal', '=', $tahun);

        $this->crud->orderBy('tanggal', 'ASC');
        $this->crud->removeAllButtons();
        $this->crud->removeColumn('action');
        $this->crud->denyAccess(['create', 'update', 'delete']);
        $this->crud->enableExportButtons();

        /*
		|--------------------------------------------------------------------------
		| COLUMNS AND FIELDS
		|--------------------------------------------------------------------------
		*/

        // ------ CRUD COLUMNS
        $this->crud->addColumn([
                                'name' => 'tanggal',
                                'label' => 'Tanggal Periksa',
                                'type' => 'date',
                                'value' => date('d F Y')
                            ]);
        $this->crud->addColumn([
                                'name' => 'hb',
                                'label' => "Hb (mg/dL)"
                            ]);
        $this->crud->addColumn([
                                'name' => 'goldar',
                                'label' => "Golongan Darah"
                            ]);
        $this->crud->addColumn([
                                'name' => 'albumin',
                                'label' => "Albumin"
                            ]);
        $this->crud->addColumn([
                                'name' => 'reduksi',
                                'label' => "Reduksi"
                            ]);
        $this->crud->addColumn([
                                'name' => 'gula',
                                'label' => "Gula Darah"
                            ]);
        $this->crud->addColumn([
                                'name' => 'urin',
                                'label' => "Urin"
                            ]);
        $this->crud->addColumn([
                                'name' => 'sifilis',
                                'label' => "Sifilis"
                            ]);
        $this->crud->addColumn([
                                'name' => 'hbsag',
                                'label' => "HbsAg"
                            ]);
        $this->crud->addColumn([
                                'name' => 'hiv',
                                'label' => "HIV"
                            ]);

        // ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date',
                                'value' => date('Y-m-d')
                            ], 'create');
        $this->crud->addField([    // TEXT
                                'name' => 'tanggal',
                                'label' => 'Tanggal',
                                'type' => 'date'
                            ], 'update');
        $this->crud->addField([    // TEXT
                                'name' => 'hb',
                                'label' => 'Hb',
                                'type' => 'number',
                                'placeholder' => 'Your title here',
                                'suffix' => "mg/dL",
                                'attributes' => ["step" => "any"], // allow decimals
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'goldar',
                                'label' => "Golongan Darah",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'albumin',
                                'label' => 'Albumin',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'reduksi',
                                'label' => 'Reduksi',
                                'type' => 'text',
                                'placeholder' => 'Your title here'
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'gula',
                                'label' => 'Gula Darah',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                                'suffix' => "mg/dL",
                            ]);
        $this->crud->addField([    // TEXT
                                'name' => 'urin',
                                'label' => 'Urin',
                                'type' => 'text',
                                'placeholder' => 'Your title here',
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'sifilis',
                                'label' => "Sifilis",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'hbsag',
                                'label' => "HbsAg",
                                'type' => 'enum'
                            ]);
        $this->crud->addField([    // ENUM
                                'name' => 'hiv',
                                'label' => "HIV",
                                'type' => 'enum'
                            ]);
    }

	public function store(StoreRequest $request)
	{
		$request['pasien_id'] = \Route::current()->parameter('pasien');

		return parent::storeCrud($request);
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}