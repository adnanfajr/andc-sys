<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class PoliLab extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'laborat';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['tanggal', 'pasien_id', 'hb', 'goldar', 'gula', 'urin', 'albumin', 'reduksi', 'sifilis', 'hbsag', 'hiv'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parent()
    {
        return $this->belongsTo('App\PoliLab', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\PoliLab', 'parent_id');
    }

    public function pasien()
    {
        return $this->hasMany('App\Pasien');
    }
/*
        public function view($crud = true)
    {
        return '<a class="btn btn-xs btn-primary" href="kia/view/'.$this->pasien_id.'"><i class="fa fa-eye"></i> Lihat detail </a>';
    }

    public function poli($entry = false)
    {
        return '<h4 style="margin: 10px">Poli lain : <h4><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->pasien_id.'/gigi" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Poli Gigi</a><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->pasien_id.'/lab" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Laboratorium</a><br>';
    }
*/
}