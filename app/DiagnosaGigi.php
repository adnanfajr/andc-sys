<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class DiagnosaGigi extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'diagnosagigis';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['kode', 'deskripsi'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
/*
    public function pasien()
    {
        return $this->hasMany('App\Pasien');
    }

    public function poligigis()
    {
        return $this->belongsToMany('App\PoliGigi', 'poligigi_diagnosagigi');
    }
*/
    public function poligigis()
    {
        return $this->hasMany('App\PoliGigi');
    }
}
