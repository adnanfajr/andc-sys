<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\TenagaMedis;

class PoliKIA extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'polikia';
    protected $primaryKey = 'id';
    public $timestamps = true;
    // protected $guarded = ['id'];
    protected $fillable = ['pasien_id', 'dokter', 'tanggal','keluhan','kehamilan','preeklamsi','bb','bb_bayi','td','nadi','rr','tfu','umur','k','lab','skor','kesimpulan','rujuk'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function parent()
    {
        return $this->belongsTo('App\PoliKIA', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\PoliKIA', 'parent_id');
    }

    public function pasien()
    {
        return $this->hasMany('App\Pasien');
    }

    public function NamaDokter()
    {
        $nama = TenagaMedis::select('nama')->where('id', $this->dokter)->first();
        $namadokter = $nama->nama;

        return $namadokter;
    }

    public function Preeklamsi()
    {
        if($this->preeklamsi == 1)
        {
            return 'Ya';
        }
        else
        {
            return 'Tidak';
        }
    }
/*
    public function view($crud = true)
    {
        return '<a class="btn btn-xs btn-primary" href="kia/view/'.$this->id.'/"><i class="fa fa-eye"></i> Lihat detail </a>';
    }

    public function poli($entry = false)
    {
        return '<h4 style="margin: 10px">Poli lain : <h4><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->id.'/gigi" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Poli Gigi</a><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->id.'/lab" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Laboratorium</a><br>';
    }
*/

}
