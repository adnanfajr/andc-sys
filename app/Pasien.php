<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;
//use Cviebrock\EloquentSluggable\Sluggable;

class Pasien extends Model
{
    use CrudTrait;
    //use Sluggable;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'pasiens';
    protected $primaryKey = 'id';
    public $timestamps = true;
    //protected $guarded = ['id', 'diagnosa'];
    protected $fillable = ['nama', 'suami', 'alamat', 'pendidikan', 'tl', 'ttl', 'usia', 'agama', 'telp', 'no_rekam_medis', 'nik', 'no_bpjs'];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     *
    *public function sluggable()
    *{
    *    return [
    *        'slug' => [
    *            'source' => 'slug_or_nama',
    *        ],
    *    ];
    *}
    */

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function diagnosagigi()
    {
        return $this->belongsToMany('App\DiagnosaGigi', 'pasien_diagnosagigi');
    }


    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    // The slug is created automatically from the "title" field if no slug exists.
    /*
    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->nama;
    }
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function kontrolKIA($crud = true)
    {
        return '<a class="btn btn-sm btn-primary" href="pasien/kontrol/'.$this->id.'/kia" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Poli KIA </a>';
    }

    public function kontrolGigi($crud = true)
    {
        return '<a class="btn btn-sm btn-primary" href="pasien/kontrol/'.$this->id.'/gigi" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Poli Gigi </a><span style="padding-right: 30px"></span>';
    }

    public function kontrolLab($crud = false)
    {
        return '<a class="btn btn-sm btn-primary" href="pasien/kontrol/'.$this->id.'/lab" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Laboratorium </a>';
    }
}
