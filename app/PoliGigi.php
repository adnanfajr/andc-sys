<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

use App\DokterGigi;
use App\DiagnosaGigi;
use DB;

class PoliGigi extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'poligigi';
    protected $primaryKey = 'id';
    public $timestamps = true;
    //protected $guarded = ['id', 'diagnosa'];
    protected $fillable = ['pasien_id', 'tanggal', 'dokter', 'tindakan', 'chart'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function diagnosa()
    {
        return $this->belongsToMany('App\DiagnosaGigi', 'poligigi_diagnosagigi');
    }

    public function tenagamedis()
    {
        return $this->belongsToMany('App\TenagaMedis');
    }

    public function parent()
    {
        return $this->belongsTo('App\PoliGigi', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\PoliGigi', 'parent_id');
    }

    public function pasien()
    {
        return $this->hasMany('App\Pasien');
    }
/*
    public function view($crud = true)
    {
        return '<a class="btn btn-xs btn-primary" href="gigi/view/'.$this->pasien_id.'"><i class="fa fa-eye"></i> Lihat detail </a>';
    }

    public function poli($entry = false)
    {
        return '<h4 style="margin: 10px">Poli lain : <h4><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->pasien_id.'/kia" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Poli KIA</a><a class="btn btn-sm btn-primary" href="/admin/pasien/kontrol/'.$this->pasien_id.'/lab" style="margin-left: 10px;margin-bottom: 20px" data-toggle="tooltip" title="Lakukan pemeriksaan pasien"><i class="fa fa-stethoscope"></i> Laboratorium</a><br>';
    }
*/
    public function NamaDokter()
    {
        $nama = TenagaMedis::select('nama')->where('id', $this->dokter)->first();
        $namadokter = $nama->nama;

        return $namadokter;
    }

    public function NamaDiagnosa()
    {
        //$diag = DiagnosaGigi::select('deskripsi')->where('id', $this->diagnosa)->first();
        //$namadiag = $diag->deskripsi;

        
        $diag = DB::select('
                SELECT GROUP_CONCAT(diagnosagigis.deskripsi) as namadiag 
                FROM poligigi_diagnosagigi 
                JOIN diagnosagigis ON poligigi_diagnosagigi.diagnosa_gigi_id = diagnosagigis.id 
                WHERE poligigi_diagnosagigi.poli_gigi_id = '.$this->id.'
                ');
        //$namadiag = $diag->namadiag;
        /*
        $diag = DB::select('
                SELECT diagnosagigis.deskripsi
                FROM diagnosagigis, poligigi, poligigi_diagnosagigi
                WHERE poligigi.id = poligigi_diagnosagigi.poli_gigi_id 
                AND poligigi_diagnosagigi.diagnosa_gigi_id = diagnosagigis.id
                ');
        //$namadiag = $diag->deskripsi;
        //$strFromArr = serialize($diag);
        //$strFromArr = implode(" ", $diag);
        
        //$namadiag = $strFromArr->deskripsi;
        
        select s.name "Student", c.name "Course"
        from student s, bridge b, course c
        where b.sid = s.sid and b.cid = c.cid 
        
        //$namadiag = $diag->poligigi_diagnosagigi.diagnosa_gigi_id;

        $users = DB::table('users')
            ->join('contacts', 'users.id', '=', 'contacts.user_id')
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.*', 'contacts.phone', 'orders.price')
            ->get();

        $dia = array_column($diag, 'deskripsi');
        
        //return $diag[0];
        //return $strFromArr;
        //return var_dump($diag);
        //return print_r($dia);
        */

        return print_r($diag[0]->namadiag);
    }

}
