@extends('backpack::layout') @section('header')
<section class="content-header">
    <h1>
        Rekapitulasi Kunjungan Pasien<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>
@endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">Pilih Data Rekapitulasi Poli</div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <h3>Grafik Rekapitulasi Keseluruhan</h3>
                    <div class="form-group">
                        <select class="form-control" name="tahun_all" id="tahun_all">
                            <option value="0" disabled selected>Pilih tahun</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                        </select>
                    </div>
                    <button class="btn btn-success" onclick="location.href='rekap/grafik/'+document.getElementById('tahun_all').value;">Lihat grafik &rarr;</button>
                
                </div>
                <div class="col-md-4" style="padding-bottom: 20px">
                    <h3>Poli KIA</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Bulan</label>
                                <select class="form-control" name="bulankia" id="bulankia">
                                    <option value="0" disabled selected>Pilih bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tahun</label>
                                <select class="form-control" name="tahunkia" id="tahunkia">
                                    <option value="0" disabled selected>Pilih tahun</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" onclick="location.href='rekap/'+document.getElementById('bulankia').value +'/'+ document.getElementById('tahunkia').value +'/kia';">Lihat data &rarr;</button>
                    <button class="btn btn-success" onclick="location.href='grafik/poli/kia/'+document.getElementById('tahunkia').value;">Lihat grafik &rarr;</button>
                    <br><br><small>**Lihat grafik hanya untuk grafik pertahun, bukan perbulan.</small>
                </div>
                <div class="col-md-4">
                    <h3>Laboratorium</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Bulan</label>
                                <select class="form-control" name="bulanlab" id="bulanlab">
                                    <option value="0" disabled selected>Pilih bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tahun</label>
                                <select class="form-control" name="tahunlab" id="tahunlab">
                                    <option value="0" disabled selected>Pilih tahun</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" onclick="location.href='rekap/'+document.getElementById('bulanlab').value +'/'+ document.getElementById('tahunlab').value +'/lab';">Lihat data &rarr;</button>
                    <button class="btn btn-success" onclick="location.href='grafik/poli/lab/'+document.getElementById('tahunlab').value;">Lihat grafik &rarr;</button>
                    <br><br><small>**Lihat grafik hanya untuk grafik pertahun, bukan perbulan.</small>
                </div>
                <div class="col-md-4">
                    <h3>Poli Gigi</h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Bulan</label>
                                <select class="form-control" name="bulangigi" id="bulangigi">
                                    <option value="0" disabled selected>Pilih bulan</option>
                                    <option value="1">Januari</option>
                                    <option value="2">Februari</option>
                                    <option value="3">Maret</option>
                                    <option value="4">April</option>
                                    <option value="5">Mei</option>
                                    <option value="6">Juni</option>
                                    <option value="7">Juli</option>
                                    <option value="8">Agustus</option>
                                    <option value="9">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tahun</label>
                                <select class="form-control" name="tahungigi" id="tahungigi">
                                    <option value="0" disabled selected>Pilih tahun</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary" onclick="location.href='rekap/'+document.getElementById('bulangigi').value +'/'+ document.getElementById('tahungigi').value +'/gigi';">Lihat data &rarr;</button>
                    <button class="btn btn-success" onclick="location.href='grafik/poli/gigi/'+document.getElementById('tahungigi').value;">Lihat grafik &rarr;</button>
                    <br><br><small>**Lihat grafik hanya untuk grafik pertahun, bukan perbulan.</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection