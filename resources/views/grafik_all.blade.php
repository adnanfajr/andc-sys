@extends('backpack::layout') @section('header')
<section class="content-header">
    <h1>
        Rekapitulasi Kunjungan Pasien<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>
@endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">Tahun {{ $tahun }}</div>
            </div>
            <div class="box-body">
                <canvas id="tahunChart" width="300" height="100"></canvas>
                <script>
                    var ctx = document.getElementById("tahunChart").getContext('2d');
                    var tahunChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: [
                                "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
                                ],
                            datasets: [
                                {
                                    label: "Pasien Poli KIA",
                                    borderColor: 'rgba(75, 192, 192, 1)',
                                    fill: false,
                                    data: [
                                        {{ $kia_jan }}, 
                                        {{ $kia_feb }}, 
                                        {{ $kia_mar }}, 
                                        {{ $kia_apr }}, 
                                        {{ $kia_mei }}, 
                                        {{ $kia_juni }}, 
                                        {{ $kia_juli }}, 
                                        {{ $kia_agus }}, 
                                        {{ $kia_sept }}, 
                                        {{ $kia_okt }}, 
                                        {{ $kia_nov }},
                                        {{ $kia_des }}
                                    ]
                                },
                                {
                                    label: "Pasien Poli Lab",
                                    borderColor: 'rgba(0, 166, 90, 1)',
                                    fill: false,
                                    data: [
                                        {{ $lab_jan }}, 
                                        {{ $lab_feb }}, 
                                        {{ $lab_mar }}, 
                                        {{ $lab_apr }}, 
                                        {{ $lab_mei }}, 
                                        {{ $lab_juni }}, 
                                        {{ $lab_juli }}, 
                                        {{ $lab_agus }}, 
                                        {{ $lab_sept }}, 
                                        {{ $lab_okt }}, 
                                        {{ $lab_nov }},
                                        {{ $lab_des }}
                                    ]
                                },
                                {
                                    label: "Pasien Poli Gigi",
                                    borderColor: 'rgba(255,99,132,1)',
                                    fill: false,
                                    data: [
                                        {{ $gigi_jan }}, 
                                        {{ $gigi_feb }}, 
                                        {{ $gigi_mar }}, 
                                        {{ $gigi_apr }}, 
                                        {{ $gigi_mei }}, 
                                        {{ $gigi_juni }}, 
                                        {{ $gigi_juli }}, 
                                        {{ $gigi_agus }}, 
                                        {{ $gigi_sept }}, 
                                        {{ $gigi_okt }}, 
                                        {{ $gigi_nov }}, 
                                        {{ $gigi_des }}
                                    ]
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
            </div>
        </div>
        
    </div>
</div>
@endsection