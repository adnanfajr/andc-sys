@extends('backpack::layout') @section('header')
<section class="content-header">
    <h1>
        Rekapitulasi Kunjungan Pasien<small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>
</section>
@endsection @section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">Tahun {{ $tahun }}</div>
            </div>
            <div class="box-body">
                <canvas id="tahunChart" width="300" height="100"></canvas>
                <script>
                    var ctx = document.getElementById("tahunChart").getContext('2d');
                    var tahunChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: [
                                "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"
                                ],
                            datasets: [{
                                    label: "Pasien Poli {{ $poli }} tahun {{ $tahun }}",
                                    borderColor: 'rgba(75, 192, 192, 1)',
                                    fill: false,
                                    data: [
                                        {{ $jan }}, 
                                        {{ $feb }}, 
                                        {{ $mar }}, 
                                        {{ $apr }}, 
                                        {{ $mei }}, 
                                        {{ $juni }}, 
                                        {{ $juli }}, 
                                        {{ $agus }}, 
                                        {{ $sept }}, 
                                        {{ $okt }}, 
                                        {{ $nov }},
                                        {{ $des }}
                                    ]
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }
                    });
                </script>
            </div>
        </div>
        
    </div>
</div>
@endsection