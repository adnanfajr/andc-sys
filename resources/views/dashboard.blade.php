@extends('backpack::layout')

@section('header')
    <section class="content-header">
      <h1>
        {{ trans('backpack::base.dashboard') }}<small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(config('backpack.base.route_prefix', 'admin')) }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">Selamat Datang dalam aplikasi <b>Ante Natal and Dental Care System (ANDC Sys)</b>!</div>
                </div>

                <div class="box-body">{{ trans('backpack::base.logged_in') }}</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="box box-success">
                <div class="box-header with-border">
                    <div class="box-title">Statistik Kunjungan Pasien : {{ date('l, d F Y') }}</span></div>
                </div>
                <div class="box-body">
                    <div class="col-md-4">
                        <h3>Poli KIA</h3>
                        <h4>Jumlah pasien hari ini : <b>{{ $kia_hari }}</b></h4>
                        <h4>Jumlah pasien minggu ini : <b>{{ $kia_minggu }}</b></h4>
                        <h4>Jumlah pasien bulan ini : <b>{{ $kia_bulan }}</b></h4>
                    </div>
                    <div class="col-md-4">
                        <h3>Laboratorium</h3>
                        <h4>Jumlah pasien hari ini : <b>{{ $lab_hari }}</b></h4>
                        <h4>Jumlah pasien minggu ini : <b>{{ $lab_minggu }}</b></h4>
                        <h4>Jumlah pasien bulan ini : <b>{{ $lab_bulan }}</b></h4>
                    </div>
                    <div class="col-md-4">
                        <h3>Poli Gigi</h3>
                        <h4>Jumlah pasien hari ini : <b>{{ $gigi_hari }}</b></h4>
                        <h4>Jumlah pasien minggu ini : <b>{{ $gigi_minggu }}</b></h4>
                        <h4>Jumlah pasien bulan ini : <b>{{ $gigi_bulan }}</b></h4>
                    </div>
                    <a class="btn btn-sm btn-default pull-right" href="/admin/rekap" style="margin: 10px">Lihat grafik rekapitulasi &rarr; </a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-success">
                <div class="box-header with-border">
                    <div class="box-title">Tentang ANDC Sys</div>
                </div>
                <div class="box-body">
                    <div class="row" style="padding: 20px; text-align: center">
                        <img src="{{ url('/') }}/img/logo-unair.png" style="width: 100px; height: 100px; margin-right: 10px"/>
                        <img src="{{ url('/') }}/img/logo-puskesmas.png" style="width: 100px; height: 100px"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
