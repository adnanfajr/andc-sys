@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <!-- <li class="header">ADMINISTRASI</li> -->
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
          
          <li class="header">PASIEN</li>
          <li><a href="{{ url('admin/pasien/create') }}"><i class="fa fa-user"></i> <span>Pendaftaran</span></a></li>
          <li><a href="{{ url('admin/pasien') }}"><i class="fa fa-stethoscope"></i> <span>Pemeriksaan</span></a></li>

          <li class="header">DATA</li>
          <li><a href="{{ url('admin/tenagamedis') }}"><i class="fa fa-user-md"></i> <span>Tenaga Medis</span></a></li>
          <li><a href="{{ url('admin/rekap') }}"><i class="fa fa-archive"></i> <span>Rekapitulasi</span></a></li>
          <!--
          <li class="treeview">
            <a href="#"><i class="fa fa-archive"></i> <span>Rekapitulasi</span> <i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
              <li><a href="{{ url('admin/rekap/kia') }}"><i class="fa fa-check-square-o"></i> <span>Poli KIA</span></a></li>
              <li><a href="{{ url('admin/rekap/lab') }}"><i class="fa fa-check-square-o"></i> <span>Laboratorium</span></a></li>
              <li><a href="{{ url('admin/rekap/gigi') }}"><i class="fa fa-check-square-o"></i> <span>Poli Gigi</span></a></li>
            </ul>
          </li>
          -->
          <li><a href="{{ url('admin/diagnosagigi') }}"><i class="fa fa-medkit"></i> <span>Diagnosa Gigi</span></a></li>

          <!-- ======================================= -->
          <li class="header">PENGGUNA</li>
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
        <!--
        <div style="padding: 20px">
          <img src="{{ url('/') }}/img/logo-unair.png" style="width: 80px; height: 80px; margin-right: 10px"/>
          <img src="{{ url('/') }}/img/logo-puskesmas.png" style="width: 80px; height: 80px"/>
        </div>
        -->
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
