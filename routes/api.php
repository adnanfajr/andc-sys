<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Routing untuk form select ajax
Route::get('/tenagamedis', 'Api\TenagaMedisController@index');
Route::get('/tenagamedis/{id}', 'Api\TenagaMedisController@show');
Route::get('/diagnosagigi', 'Api\DiagnosaGigiController@index');
Route::get('/diagnosagigi/{id}', 'Api\DiagnosaGigiController@show');
