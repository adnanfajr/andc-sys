<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => ['web']], function()
{
    Route::get('/', function () {
        return redirect('admin/login');
    });
    Auth::routes();
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('dashboard', 'DashboardController@dashboard');
    CRUD::resource('pasien', 'PasienCrudController');
    CRUD::resource('pasien/kontrol/{pasien}/kia', 'KIACrudController');
    CRUD::resource('pasien/kontrol/{pasien}/gigi', 'GigiCrudController');
    CRUD::resource('pasien/kontrol/{pasien}/lab', 'LabCrudController');
    CRUD::resource('tenagamedis', 'TenagaMedisCrudController');
    CRUD::resource('diagnosagigi', 'DiagnosaGigiCrudController');
    Route::get('rekap', function () {
        return view('rekap');
    });
    CRUD::resource('rekap/{bulan}/{tahun}/kia', 'RekapKIACrudController');
    CRUD::resource('rekap/{bulan}/{tahun}/lab', 'RekapLabCrudController');
    CRUD::resource('rekap/{bulan}/{tahun}/gigi', 'RekapGigiCrudController');
    Route::get('rekap/grafik/{tahun}', 'DashboardController@rekap');
    Route::get('grafik/poli/{poli}/{tahun}', 'DashboardController@rekap_poli');
    /*
    Route::get('rekap/kia', 'DashboardController@rekapKIA');
    Route::get('rekap/lab', 'DashboardController@rekapLab');
    Route::get('rekap/gigi', 'DashboardController@rekapGigi');
    */
});
