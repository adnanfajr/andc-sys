<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
        // Admin
        ['name'=>'Admin', 'email'=>'admin@andc-sys.com','password'=>bcrypt('rahasia')]
        ];

        // masukkan data ke database
        DB::table('users')->insert($user);
    }
}
