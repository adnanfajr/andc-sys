<?php

use Illuminate\Database\Seeder;

class TenagaMedisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $med = [
            // Dokter
            ['nama'=>'dr. Dedy Nurmantoro', 'pekerjaan'=>'Dokter'],
            ['nama'=>'dr. Ani Rachmawati', 'pekerjaan'=>'Dokter'],
            ['nama'=>'dr. Faradilla Rahmy S.', 'pekerjaan'=>'Dokter'],
            ['nama'=>'dr. Ayu Ekanita', 'pekerjaan'=>'Dokter'],
            
            // Dokter Gigi
            ['nama'=>'drg. Umi Latifah', 'pekerjaan'=>'Dokter Gigi'],
            ['nama'=>'drg. Ria Pramita Sari', 'pekerjaan'=>'Dokter Gigi'],
            ['nama'=>'drg. Prasukma Yogawarti', 'pekerjaan'=>'Dokter Gigi'],

            // Perawat
            ['nama'=>'Tantri K.,Amd.Kep', 'pekerjaan'=>'Perawat'],
            ['nama'=>'M. Deni KM.,Amd.Kep', 'pekerjaan'=>'Perawat'],
            ['nama'=>'M. Ashadi M.,S.Kep.Ns', 'pekerjaan'=>'Perawat'],
            ['nama'=>'Fauca R.,Amd.Kep', 'pekerjaan'=>'Perawat'],
            ['nama'=>'Surya F.,Amd.Kep', 'pekerjaan'=>'Perawat'],

            // Bidan
            ['nama'=>'Sri Rahayu,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Patria K. W.,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Eti Kumalasari,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Patria Kesuma,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Dian Wahyu,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Dian Anggarani,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Mulia Ambar H.,Amd.Keb', 'pekerjaan'=>'Bidan'],
            ['nama'=>'Neng Wulan,S.Tr.Keb', 'pekerjaan'=>'Bidan'],
        ];

        // masukkan data ke database
        DB::table('tenagamedis')->insert($med);
    }
}
