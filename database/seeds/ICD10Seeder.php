<?php

use Illuminate\Database\Seeder;

class ICD10Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $diag = [
        // Admin
        ['kode'=>'K00.1', 'deskripsi'=>'Supernumerary teeth'],
        ['kode'=>'K00.2', 'deskripsi'=>'Abnormalities of size and form of teeth'],
        ['kode'=>'K00.3', 'deskripsi'=>'Mottled teeth'],
        ['kode'=>'K00.4', 'deskripsi'=>'Disturbances in tooth formation '],
        ['kode'=>'K00.5', 'deskripsi'=>'Hereditary disturbances in tooth structure, not elsewhere classified'],
        ['kode'=>'K00.6', 'deskripsi'=>'Disturbances in tooth eruption'],
        ['kode'=>'K01.1', 'deskripsi'=>'Embedded teeth'],
        ['kode'=>'K01.2', 'deskripsi'=>'Impacted teeth'],
        ['kode'=>'K02.61', 'deskripsi'=>'Dental caries on smooth surface limited to enamel'],
        ['kode'=>'K02.62', 'deskripsi'=>'Dental caries on smooth surface penetrating into dentin'],
        ['kode'=>'K03.0', 'deskripsi'=>'Excessive attrition of teeth'],
        ['kode'=>'K03.1', 'deskripsi'=>'Abrasion of teeth'],
        ['kode'=>'K03.2', 'deskripsi'=>'Erosion of teeth'],
        ['kode'=>'K03.3', 'deskripsi'=>'Pathological resorption of teeth'],
        ['kode'=>'K03.6', 'deskripsi'=>'Deposits [accretions] on teeth'],
        ['kode'=>'K04.1', 'deskripsi'=>'Necrosis of pulp'],
        ['kode'=>'K04.2', 'deskripsi'=>'Pulp degeneration'],
        ['kode'=>'K04.3', 'deskripsi'=>'Abnormal hard tissue formation in pulp'],
        ['kode'=>'K04.4', 'deskripsi'=>'Acute apical periodontitis of pulpal origin'],
        ['kode'=>'K04.7', 'deskripsi'=>'Periapical abscess without sinus'],
        ['kode'=>'K05.4', 'deskripsi'=>'Periodontosis'],
        ['kode'=>'K05.5', 'deskripsi'=>'Other periodontal diseases'],
        ['kode'=>'K06.1', 'deskripsi'=>'Gingival enlargement'],
        ['kode'=>'K06.2', 'deskripsi'=>'Gingival and edentulous alveolar ridge lesions associated with trauma'],
        ['kode'=>'K08.419', 'deskripsi'=>'Partial loss of teeth due to trauma, unspecified class'],
        ['kode'=>'K08.429', 'deskripsi'=>'Partial loss of teeth due to periodontal diseases, unspecified class'],
        ['kode'=>'K08.439', 'deskripsi'=>'Partial loss of teeth due to caries, unspecified class'],
        ['kode'=>'K09.1', 'deskripsi'=>'Developmental (nonodontogenic) cysts of oral region'],
        ['kode'=>'M26.01', 'deskripsi'=>'Maxillary hyperplasia'],
        ['kode'=>'M26.02', 'deskripsi'=>'Maxillary hypoplasia'],
        ['kode'=>'M26.03', 'deskripsi'=>'Mandibular hyperplasia'],
        ['kode'=>'M26.04', 'deskripsi'=>'Mandibular hypoplasia'],
        ['kode'=>'M27.51', 'deskripsi'=>'Perforation of root canal space due to endodontic treatment'],
        ['kode'=>'M27.52', 'deskripsi'=>'Endodontic overfill'],
        ['kode'=>'Z01.20', 'deskripsi'=>'Encounter for dental examination and cleaning without abnormal findings'],
        ['kode'=>'Z01.21', 'deskripsi'=>'Encounter for dental examination and cleaning with abnormal findings'],
        ['kode'=>'Z46.4', 'deskripsi'=>'Encounter for fitting and adjustment of orthodontic device'],
        ];

        // masukkan data ke database
        DB::table('diagnosagigis')->insert($diag);
    }
}
