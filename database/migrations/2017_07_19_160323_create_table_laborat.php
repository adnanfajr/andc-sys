<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLaborat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laborat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pasien_id')->unsigned();
            $table->date('tanggal');
            $table->float('hb')->nullable();
            $table->enum('goldar', ['A', 'B', 'AB', 'O'])->default('A');
            $table->string('albumin')->nullable();
            $table->string('reduksi')->nullable();
            $table->string('gula')->nullable();
            $table->string('urin')->nullable();
            $table->enum('sifilis', ['Negatif', 'Positif'])->default('Negatif');
            $table->enum('hbsag', ['Negatif', 'Positif'])->default('Negatif');
            $table->enum('hiv', ['Negatif', 'Positif'])->default('Negatif');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laborat');
    }
}
