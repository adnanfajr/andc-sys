<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDoktergigi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenagamedis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->enum('pekerjaan', ['Dokter', 'Dokter Gigi', 'Bidan', 'Perawat'])->default('Dokter');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenagamedis');
    }
}
