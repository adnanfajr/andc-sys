<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePasien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('suami');
            $table->string('tl');
            $table->date('ttl');
            $table->string('pendidikan');
            $table->text('alamat');
            $table->enum('agama', ['Islam', 'Kristen Protestan', 'Kristen Katolik', 'Hindu', 'Buddha', 'Khonghuchu', 'Lain-lain'])->default('Lain-lain');
            $table->integer('usia');
            $table->string('telp');
            $table->string('no_rekam_medis');
            $table->string('nik');
            $table->string('no_bpjs');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
