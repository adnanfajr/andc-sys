<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePolikia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polikia', function (Blueprint $table) {
            $table->increments('id');
            $table->text('pasien_id');
            $table->string('k');
            $table->date('tanggal');
            $table->integer('dokter')->unsigned();
            $table->integer('kehamilan'); 
            $table->boolean('preeklamsi')->default(0); // ceklis riwayat preeklamsi kehamilan sebelumnya -> gak harus
            $table->string('keluhan');
            $table->integer('bb')->nullable();
            $table->integer('bb_bayi')->nullable();
            $table->string('td')->nullable();
            $table->float('nadi')->nullable();
            $table->float('rr')->nullable();
            $table->string('tfu')->nullable();
            $table->string('umur');
            $table->integer('skor')->nullable();
            $table->text('kesimpulan')->nullable();
            $table->string('rujuk')->nullable(); // dikasih ceklis gigi sama lab
            $table->enum('status', ['Pilih status', 'Murni', 'Faskes'])->default('Pilih status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polikia');
    }
}
